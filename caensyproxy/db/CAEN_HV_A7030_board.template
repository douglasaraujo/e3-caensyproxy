# file: CAEN_HV_A7030_board.template
# description: EPICS template to connect CAEN A7030 IOC PV. This is the PV for the board management (not channels).
# author: victor.nadot@cea.fr
# company: CEA / DRF / IRFU / LDISC
# macros:
# > Mandatory
# P: ${P=}                      | PV name: ${P}${R}...
# R: ${R=}                      | PV name: ${P}${R}...
# CRATE_PREFIX: ${CRATE_PREFIX} | prefix of the PV name of CAEN crate IOC
# HV_SLOT: ${HV_SLOT}           | HV slot of the PV name of CAEN crate IOC
# > Optional
#

# CAEN HV board, A7030

# board status (word)
record(ai, "${P}${R}BdStatus"){
	field(DESC, "board sataus")
    field(INP,  "${CRATE_PREFIX}:${HV_SLOT}:BdStatus CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# board status (bit)
# bits 0 to 7: alarm status
record(calcout, "${P}${R}boardMainStatus"){
    field(DESC, "HV board status")
    field(INPA, "${P}${R}BdStatus CP MSS") # MSS (Maximize Status and Severity): alarm propagation
    field(HIGH,"1")
    field(HSV,"MAJOR")
    field(CALC, "(a&1)|(a>>1)&1|(a>>2)&1|(a>>3)&1|(a>>4)&1|(a>>5)&1|(a>>6)&1|(a>>7)&1")
}

# board max voltage
record(ai, "${P}${R}HVMax"){
	field(DESC, "board max voltage")
    field(EGU,  "V")
    field(INP,  "${CRATE_PREFIX}:${HV_SLOT}:HVMax CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# board max current
record(ai, "${P}${R}HIMax"){
	field(DESC, "board max current")
    field(EGU, "uA")
    field(INP, "${CRATE_PREFIX}:${HV_SLOT}:HIMax CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# board temperature
record(ai, "${P}${R}Temp"){
	field(DESC, "board temperature")
    field(EGU, "°c")
    field(INP, "${CRATE_PREFIX}:${HV_SLOT}:Temp CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}


# clear alarms
record(bo, "${P}${R}ClrAlarm"){
	field(DESC, "clear board alarms")
    field(OUT,"${CRATE_PREFIX}:${HV_SLOT}:ClrAlarm PP")
    field(VAL, "0")
}
record(bi, "${P}${R}ClrAlarm-RB"){
	field(DESC, "clear board alarms RB")
	field(INP,  "${CRATE_PREFIX}:${HV_SLOT}:ClrAlarm CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

