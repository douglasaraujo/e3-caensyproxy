# file: CAEN_LV_A2519_board.template
# description: EPICS template to connect CAEN A2519 IOC PV. This is the PV for the board management (not channels).
# author: victor.nadot@cea.fr
# company: CEA / DRF / IRFU / LDISC
# macros:
# > Mandatory
# P: ${P=}                      | PV name: ${P}${R}...
# R: ${R=}                      | PV name: ${P}${R}...
# CRATE_PREFIX: ${CRATE_PREFIX} | prefix of the PV name of CAEN crate IOC
# LV_SLOT: ${LV_SLOT}           | LV slot of the PV name of CAEN crate IOC
# > Optional
#

# CAEN LV board, A2519

# lv board status (word)
record(ai, "${P}${R}BdStatus"){
	field(DESC, "board sataus")
    field(INP,  "${CRATE_PREFIX}:${LV_SLOT}:BdStatus CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# lv board status (bit)
# bits 0 to 7: alarm status
record(calcout, "${P}${R}boardMainStatus"){
    field(DESC, "LV board main status")
    field(INPA, "${P}${R}BdStatus CP MSS") # MSS (Maximize Status and Severity): alarm propagation
	field(HIGH,"1")
    field(HSV,"MAJOR")
    field(CALC, "(a&1)|(a>>1)&1|(a>>2)&1|(a>>3)&1|(a>>4)&1|(a>>5)&1|(a>>6)&1|(a>>7)&1")
}

# clear alarms
record(bo, "${P}${R}ClrAlarm"){
	field(VAL, "0")
	field(DESC, "clear board alarms")
    field(OUT,"${CRATE_PREFIX}:${LV_SLOT}:ClrAlarm PP")
}
record(bi, "${P}${R}ClrAlarm-RB"){
	field(DESC, "clear board alarms RB")
	field(INP,  "${CRATE_PREFIX}:${LV_SLOT}:ClrAlarm CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# interlock A
record(bo, "${P}${R}IntckA"){
	field(VAL, "0")
	field(DESC, "interlock A, ch0 to ch3")
    field(OUT,"${CRATE_PREFIX}:${LV_SLOT}:IntckA PP")
}
record(bi, "${P}${R}IntckA-RB"){
	field(DESC, "interlock A, ch0 to ch3 RB")
	field(INP,  "${CRATE_PREFIX}:${LV_SLOT}:IntckA CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# interlock B
record(bo, "${P}${R}IntckB"){
	field(VAL, "0")
	field(DESC, "interlock V, ch4 to ch7")
    field(OUT,"${CRATE_PREFIX}:${LV_SLOT}:IntckB PP")
}
record(bi, "${P}${R}IntckB-RB"){
	field(DESC, "interlock A, ch4 to ch7 RB")
	field(INP,  "${CRATE_PREFIX}:${LV_SLOT}:IntckB CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}


# switch on group
record(ao, "${P}${R}OnGroup"){
	field(DESC, "switch on group")
    field(OUT,  "${CRATE_PREFIX}:${LV_SLOT}:OnGroup PP")
}
# RB
record(ai, "${P}${R}OnGroup-RB"){
	field(DESC, "switch on group RB")
	field(INP,  "${CRATE_PREFIX}:${LV_SLOT}:OnGroup CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}

# switch off group
record(ao, "${P}${R}OffGroup"){
	field(DESC, "switch off group")
    field(OUT,"${CRATE_PREFIX}:${LV_SLOT}:OffGroup PP")
}
# RB
record(ai, "${P}${R}OffGroup-RB"){
	field(DESC, "switch off group RB")
	field(INP,  "${CRATE_PREFIX}:${LV_SLOT}:OffGroup CP MSS") # MSS (Maximize Status and Severity): alarm propagation
}



