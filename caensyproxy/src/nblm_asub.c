#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <math.h>


// This function split Caen power supply status in 2 pV: warning and alarm
static int PSchannelMainStatus(aSubRecord *precord) {
    short powerSupplyStatus = *(short *)precord->a;   // 16 bits word

    short warning   = 0;
    short alarm     = 0;
    // The following messages may be returned by the SYSTEM when monitoring the channel STATUS:
    // 1: RUP channel ramping up
    // 2: RDWN channel ramping down
    // WARNING
    // OVC channel in OVERCURRENT condition
    // OVV channel in OVERVOLTAGE condition
    // UNV channel in UNDERVOLTAGE condition
    // VMAX channel reached VMAX condition
    // ALARM
    // E-TRIPPED channel OFF due to external TRIP line signal
    // I-TRIPPED channel OFF due to internal OVERCURRENT condition
    // EXT_DIS channel disabled by board INTERLOCK protection
    // PWR_FAIL channel OFF due to exceeded power limit (>1.5W)

    // printf("powerSupplyStatus:%d\n", powerSupplyStatus);

    warning     = (powerSupplyStatus>>3 | powerSupplyStatus>>4 | powerSupplyStatus>>5 | powerSupplyStatus>>10 | powerSupplyStatus>>11) & 0x1;    // sum of all warning bits
    alarm       = (powerSupplyStatus>>6 | powerSupplyStatus>>7 | powerSupplyStatus>>8 | powerSupplyStatus>>8) & 0x1;                    // sum of all alarm bits

    // printf("warning:%d\n", warning);
    // printf("alarm:%d\n", alarm);

    // output
    *(short *)precord->vala = warning | alarm; // <<1;

    return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(PSchannelMainStatus);


// turn off HV when LV is off
static int CAENsoftInterlock(aSubRecord *precord) {
    // constant
    short LV_OFF = 0;

    // inputs
    short LVposStat = *(short *)precord->a;
    short LVnegStat = *(short *)precord->b;  
    short HVmeshPw  = *(short *)precord->c;
    short HVdriftPw = *(short *)precord->d;

    if ( (LVposStat&0x1) == LV_OFF || (LVnegStat&0x1) == LV_OFF ){
        // LV (at least one) is off: disable HVs
        *(short *)precord->vala = 0; 
        *(short *)precord->valb = 0; 
        // printf("DISABLE HV channels because LV is off\n");
    } else {
        // LV are on: do nothing
        *(short *)precord->vala = HVmeshPw; 
        *(short *)precord->valb = HVdriftPw; 
        // printf("do nothing\n");
    }

    return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(CAENsoftInterlock);