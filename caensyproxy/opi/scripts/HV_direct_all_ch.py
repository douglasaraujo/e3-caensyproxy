# For all SY4527 board channels, set the same value (ex: voltage)
import epics
import sys
import time

## defines
# HV board (caen A7030) has 48 channels
NB_OF_CHANNEL       = 48
# dat to set
POWER_ON            = 0
CURRENT             = 1
CURRENT_OFSSET      = 2
VOLTAGE             = 3
VOLTAGE_MAX         = 4
VOLTAGE_RAMP_UP     = 5
VOLTAGE_RAMP_DOWN   = 6
RESTORE_CONF        = 7
RAMP_ON_KILL        = 8
TRIP                = 9
# timeout
TIMEOUT_CONNECTION  = 1     # sec
TIMEOUT_CAPUT       = 30    # sec
TIMEOUT_CAGET       = 30    # sec

## check script args
if len(sys.argv) != 4:
    print "FAILED: wrong number of args. use: python <script_name> <prefix_SY4527> <data_to_set: [0-6]>"
    exit()
else:
    # get prefix SY4527
    prefixSY4527 = sys.argv[1]
    print "prefix SY4527:", prefixSY4527

    # get board number
    boardNumber         = int(sys.argv[2])
    boardNumberString   = "{:02d}".format(boardNumber)
    print "boardNumber:", boardNumber

    # get data to set
    dataToSet = int(sys.argv[3])
    print "data to set:", dataToSet
    if dataToSet == POWER_ON:
        signal = "Pw"
    elif dataToSet == CURRENT:
        signal = "I0Set"
    elif dataToSet == CURRENT_OFSSET:
        signal = "ImAdj"
    elif dataToSet == VOLTAGE:
        signal = "V0Set"
    elif dataToSet == VOLTAGE_MAX:
        signal = "SVMax"
    elif dataToSet == VOLTAGE_RAMP_UP:
        signal = "RUp"
    elif dataToSet == VOLTAGE_RAMP_DOWN:
        signal = "RDWn"
    elif dataToSet == RESTORE_CONF:
        signal = "PDwn"
    elif dataToSet == RAMP_ON_KILL:
        signal = "POn"
    elif dataToSet == TRIP:
        signal = "Trip"
    else:
        exit("FAILED: wrong choice data to set")

# get value to set
PREFIX      = "CEA"
SUFFIX      = "ALL_CHANNELS"
# pvName ex: IFC1410_nBLM:ALL_CHANNELS:I0Set
pvName      = PREFIX + ":" + SUFFIX + ":" + signal 
pv          = epics.PV(pvName, connection_timeout=TIMEOUT_CONNECTION)
valueToSet  = pv.get(timeout=TIMEOUT_CAGET)

print "All channels of board", boardNumber, "are going to be set to", valueToSet

# set for all channels of the board
for channel in range (NB_OF_CHANNEL):
    # int to string
    chString = "{:03d}".format(channel)
    # print "ch:", chString

    # connect to PV channel
    SERVICE_NAME= "SY4527"
    # pvName ex: SY4527:02:000:V0Set
    pvName      = SERVICE_NAME + ":" + boardNumberString + ":" + chString + ":" + signal
    pv          = epics.PV(pvName, connection_timeout=TIMEOUT_CONNECTION)

    # check connection
    if pv.wait_for_connection(timeout=TIMEOUT_CONNECTION) == False:
        # connection ko
        print "FAILED PV connection:", pvName
        exit()

    else:
        # connection ok

        # wait caput is effective
        valueSet = False
        while not valueSet:
            # caput value
            pv.put(valueToSet, wait=True, timeout=TIMEOUT_CAPUT)

            if pv.get(timeout=TIMEOUT_CAGET) != valueToSet:
                valueSet = False
                # time.sleep(0.1)
                # print "[W]: value not set"
                # bug
                # I don't understand why I should some times caput is not taken
                # I have to do several caput
                # but not grave for test prupose
            else:
                valueSet = True

        # confirm caput in prompt
        print pvName, "\t->", valueToSet

    # tempo between caput
    # without it, some caput are ignored
    time.sleep(0.1)
