importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
 
var macroName   = PVUtil.getString(pvArray[0]); // macro name
var macroValue  = PVUtil.getString(pvArray[1]); // macro value
// ConsoleUtil.writeInfo("macroName"+ macroName);
// ConsoleUtil.writeInfo("macroValue"+ macroValue);
 
// create a new macro structure and insert
// var myMacros = DataUtil.createMacrosInput(true);

// get current macro
myMacros = widget.getPropertyValue("macros");

// add new macro
myMacros.put(macroName, macroValue);
// ConsoleUtil.writeInfo("myMacros"+ myMacros);

// set new macro
widget.setPropertyValue("macros", myMacros);

// reload the embedded OPI
OPItoReload = widget.getPropertyValue("opi_file");
// ConsoleUtil.writeInfo("OPItoReload"+ OPItoReload);
widget.setPropertyValue("opi_file", "");
widget.setPropertyValue("opi_file", OPItoReload);



