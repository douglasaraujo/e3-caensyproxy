import epics
import sys

def set_channels(channels, output):
        if(output == "1"): #card enable
        	for i in xrange(0, len(channels)):
			channels[i].put('1') #channel = ON	
	else: # card disable 
		for i in xrange(0, len(channels)):
        		channels[i].put('0') #channel = OFF
 

# get argument
print 'sys.argv: ', sys.argv
if len(sys.argv) > 1:
	for i in xrange(1, len(sys.argv)):
		print(sys.argv[i])
else:
	print('No argument')

channels = []
#HV1
if(sys.argv[1] == "HV1"):
        	
	# PV connection
	channels.append(epics.PV('SEDI_TEST:HV1:CH1')) #channel 1/48
	channels.append(epics.PV('SEDI_TEST:HV1:CH2')) #channel 2/48
	#...
	print channels

	# set 48 channels on or off
	set_channels(channels, sys.argv[2]);

#HV2
elif(sys.argv[1] == "HV2"):
        	
	# PV connection
	channels.append(epics.PV('xxxxx')) #channel 1/48
	channels.append(epics.PV('xxxxx')) #channel 2/48
	print channels

	# set 48 channels on or off
	set_channels(channels, sys.argv[2]);

#HV3
elif(sys.argv[1] == "HV3"):
        	
	# PV connection
	channels.append(epics.PV('xxxxx')) #channel 1/48
	channels.append(epics.PV('xxxxx')) #channel 2/48
	print channels

	# set 48 channels on or off
	set_channels(channels, sys.argv[2]);


#HV4
elif(sys.argv[1] == "HV4"):
        	
	# PV connection
	channels.append(epics.PV('xxxxx')) #channel 1/48
	channels.append(epics.PV('xxxxx')) #channel 2/48
	print channels

	# set 48 channels on or off
	set_channels(channels, sys.argv[2]);
